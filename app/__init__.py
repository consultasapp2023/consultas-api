from flask import Flask

app = Flask(__name__)


from app.init import intflask
from app.email import send_email
from app.pacientes import create, get_patients,update,delete , get_by_name ,images , get_by_id
from app.user import user_register ,user_login ,images_storage, recover_password, change_password
from app.consult import create_consult, consult_get_by_id_patients, update_consult, delete_consult
from app.line import create_line
from app.treatment import treatment
from app.cabalga import create_cord