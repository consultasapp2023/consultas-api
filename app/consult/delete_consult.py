from flask import jsonify
from app import app
from app.utils.firebase import firebase_db


@app.route('/consult/<string:consult_id>', methods=['DELETE'])
def delete_consult(consult_id):
    try:
        # Intentar obtener el registro antes de eliminarlo para verificar si existe
        consult_ref = firebase_db.get('/consult', consult_id)
        if consult_ref is None:
            return jsonify({'message': 'No patient found with the provided ID'}), 404

        # Eliminar el registro
        firebase_db.delete('/consult', consult_id)
        return jsonify({'message': f'Consult record with ID {consult_id} deleted successfully'})

    except Exception as e:
        app.logger.error(f"Error: {str(e)}")  # Registrar el error en los logs del servidor
        return jsonify({'error': str(e)}), 500