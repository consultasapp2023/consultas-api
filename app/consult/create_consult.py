from datetime import datetime
from flask import jsonify, request
from app import app
from app.utils.firebase import firebase_db


@app.route('/consult/create_consult', methods=['POST'])
def create_consult():
    try:
        data = request.get_json()
        data['creation_date'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        result = firebase_db.post('/consult', data)
        return jsonify({'message': 'consult record created successfully', 'id': result['name']})
    except Exception as e:
        return jsonify({'error': str(e)}), 500
