from datetime import datetime
from flask import jsonify, request
from app import app
from app.utils.firebase import firebase_db


@app.route('/consult/<string:consult_id>', methods=['PATCH'])
def update_consult_partial(consult_id):
    try:
        data = request.get_json()
        data['last_updated'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        firebase_db.put('/consult', consult_id, data)

        return jsonify({'message': 'Consult record updated successfully', 'id': consult_id})
    except Exception as e:
        return jsonify({'error': str(e)}), 500