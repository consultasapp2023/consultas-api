
from flask import jsonify
from app import app
from app.utils.firebase import firebase_db


@app.route('/consult/<string:id_patient>', methods=['GET'])
def search_consult_by_id_patient(id_patient):
    try:
        if not id_patient:
            return jsonify({'error': 'The "id_patient" parameter is required in the URL'}), 400

        consults = firebase_db.get('/consult', None)
        matching_patients = []

        if consults:
            for consult_id, consult in consults.items():

                if consult.get('id_patient', '').lower() == id_patient.lower():
                    consult['consult_id'] = consult_id
                    matching_patients.append(consult)

        if matching_patients:
            return jsonify(matching_patients)
        else:
            return jsonify({'message': 'No patients found with the specified ID'}), 404

    except Exception as e:
        return jsonify({'error': str(e)}), 500



