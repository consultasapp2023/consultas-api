from flask import jsonify
from app import app
from app.utils.firebase import firebase_db


@app.route('/patient/<string:idUser>', methods=['GET'])
def by_id_patient(idUser):
    try:
        if not idUser:
            return jsonify({'error': 'The "patient" parameter is required in the URL'})

        patients = firebase_db.get('/patients', None)
        matching_patients = []

        if patients:
            for key, patient in patients.items():
                if 'idUser' in patient and idUser.lower() in patient['idUser'].lower():
                    patient_with_id = {'id': key, **patient}
                    matching_patients.append(patient_with_id)

        if matching_patients:
            return jsonify(matching_patients)
        else:
            return jsonify({'message': 'No patients found with the specified id'})

    except Exception as e:
        return jsonify({'error': str(e)}), 500