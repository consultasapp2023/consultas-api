from flask import jsonify
from app import app
from app.utils.firebase import firebase_db


@app.route('/patient/get_patients', methods=['GET'])
def get_patients():
    try:
        result = firebase_db.get('/patients', None)
        if result:
            patients = []
            for patient_id, patient_data in result.items():
                patient_data['id'] = patient_id
                patients.append(patient_data)
            return jsonify(patients)
        else:
            return jsonify([])  # Returns an empty list if there are no patients
    except Exception as e:
        return jsonify({'error': str(e)}), 500





