from datetime import datetime
from app.utils.firebase import firebase_db

from flask import request, jsonify
from app import app


@app.route('/patient/update/<string:id_patients>', methods=['POST'])
def update_patient(id_patients):
    try:
        data = request.get_json()
        data['last_updated'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        firebase_db.put('/patients', id_patients, data)

        return jsonify({'message': 'patients record updated successfully', 'id': id_patients})
    except Exception as e:
        return jsonify({'error': str(e)}), 500