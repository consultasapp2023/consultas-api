from flask import jsonify
from app import app
from app.utils.firebase import firebase_db

@app.route('/patient/delete_patient/<string:id>', methods=['DELETE'])
def delete_patient(id):
    try:
        result = firebase_db.delete('/patients', id)
        if result:
            return jsonify({'message': 'Patient record deleted successfully'})
        else:
            return jsonify({'message': 'No patient found with the provided ID'})
    except Exception as e:
        print(f"Error: {str(e)}")
        return jsonify({'error': str(e)}), 500