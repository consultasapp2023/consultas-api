from flask import request, jsonify
from cloudinary.uploader import upload
from cloudinary import uploader
from app import app


@app.route('/patient/upload-images', methods=['POST'])
def upload_image():
    if 'image' not in request.files:
        return jsonify({"error": "No image provided"})

    image = request.files['image']

    if image.filename == '':
        return jsonify({"error": "Invalid file name"})

    # Define the folder in Cloudinary where you want to save the image
    folder = "patients"  # Replace "your_folder" with the desired folder name

    # Upload the image to Cloudinary in the specified folder
    result = upload(image, folder=folder)

    image_url = result['secure_url']

    return jsonify({"url": image_url}), 200


@app.route('/patient/delete-image/<string:id_images>', methods=['DELETE'])
def delete_image(id_images):
    image_identifier = id_images

    if not image_identifier:
        return jsonify({"error": "Image identifier not provided"}), 400

    try:
        # Construct the image URL in Cloudinary, which includes the "patients" folder name.
        image_url = f'patients/{image_identifier}'

        # Attempt to delete the image from Cloudinary
        result = uploader.destroy(image_url)
        if result.get('result') == 'ok':
            return jsonify({"message": "Image deleted successfully"}), 200
        else:
            return jsonify({"error": "Failed to delete the image"}), 400
    except Exception as e:
        return jsonify({"error": str(e)}), 500
