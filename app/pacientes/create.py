import secrets
from datetime import datetime
from app.utils.firebase import firebase_db

from flask import request, jsonify
from cloudinary.uploader import upload
from app import app


@app.route('/patient/create_patient', methods=['POST'])
def create_patient():
    try:
        data = request.get_json()
        data['creation_date'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        result = firebase_db.post('/patients', data)
        return jsonify({'message': 'Patient record created successfully', 'id': result['name']})
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@app.route('/patient/create', methods=['POST'])
def create_patient_with_image():
    # Verificar si se envió una imagen en el formulario
    if 'image' not in request.files:
        return jsonify({"error": "No image provided"})

    image = request.files['image']

    # Verificar si se proporcionó un nombre de archivo válido para la imagen
    if image.filename == '':
        return jsonify({"error": "Invalid file name"})

    # Define el folder en Cloudinary donde deseas guardar la imagen
    folder = "patients"  # Reemplaza "your_folder" con el nombre de carpeta deseado

    # Subir la imagen a Cloudinary en el folder especificado
    result = upload(image, folder=folder)

    image_url = result['secure_url']

    # Obtener otros datos del formulario
    name = request.form.get("name")
    age = request.form.get("age")
    idUser = request.form.get("idUser")
    lastName = request.form.get("lastName")
    phone = request.form.get("phone")
    kg = request.form.get("kg")
    statud = request.form.get("statud")
    coments = request.form.get("coments")
    email = request.form.get("email")


    # Llamar a create_patient_data() con la URL de la imagen y otros datos del paciente
    data = {
        "name": name,
        "age": age,
        "idUser": idUser,
        "lastName": lastName,
        "phone": phone,
        "kg": kg,
        "statud": statud,
        "coments": coments,
        "image_url": image_url,
        "email": email
    }
    response = create_patient_data(data)

    return response


def create_patient_data(data):
    try:
        data['creation_date'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        result = firebase_db.post('/patients', data)
        return jsonify({'message': 'Patient record created successfully', 'id': result['name']})
    except Exception as e:
        return jsonify({'error': str(e)}), 500
