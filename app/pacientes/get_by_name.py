from flask import jsonify, request
from app import app
from app.utils.firebase import firebase_db


@app.route('/patient/search_patients_by_name', methods=['GET'])
def search_patients_by_name():
    try:
        # Get the name from the URL
        name = request.args.get('name')

        if not name:
            return jsonify({'error': 'The "name" parameter is required in the URL'})

        patients = firebase_db.get('/patients', None)
        matching_patients = []

        if patients:
            for key, patient in patients.items():
                if 'name' in patient:
                    if name.lower() in patient['name'].lower():
                        matching_patients.append(patient)

        if matching_patients:
            return jsonify(matching_patients)
        else:
            return jsonify({'message': 'No patients found with the specified name'})

    except Exception as e:
        return jsonify({'error': str(e)}), 500
