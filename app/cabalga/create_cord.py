from datetime import datetime
from app.utils.firebase import firebase_db

from flask import request, jsonify
from app import app


@app.route('/cord', methods=['POST'])
def create_cord():
    try:
        data = request.get_json()

        result = firebase_db.post('/cord', data)
        return jsonify({'message': ' line create success successfully', 'id': result['name']})
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@app.route('/cord/<string:cord_id>', methods=['PUT'])
def edit_cord(cord_id):
    try:
        data = request.get_json()
        data['last_modified_date'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')



        # Realiza una solicitud PUT para actualizar el registro en Firebase
        firebase_db.put('/cord', cord_id, data)

        return jsonify({'message': f'se actualizo correctamente'})
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@app.route('/cord/<string:cord_id>', methods=['GET'])
def get_cord(cord_id):
    try:
        cord = firebase_db.get('/cord', cord_id)
        if cord:
            return jsonify(cord)
        else:
            return jsonify({'error': 'Linea no encontrada'}), 404
    except Exception as e:
        return jsonify({'error': str(e)}), 500
