from datetime import datetime
from app.utils.firebase import firebase_db

from flask import request, jsonify
from app import app


@app.route('/treatment', methods=['POST'])
def create_treatment():
    try:
        data = request.get_json()
        data['creation_date'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        result = firebase_db.post('/treatment', data)
        return jsonify({'message': 'treatment record created successfully', 'id': result['name']})
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@app.route('/treatment', methods=['GET'])
def get_treatment():
    try:
        result = firebase_db.get('/treatment', None)
        if result:
            patients = []
            for patient_id, patient_data in result.items():
                patient_data['id'] = patient_id
                patients.append(patient_data)
            return jsonify(patients)
        else:
            return jsonify([])  # Returns an empty list if there are no patients
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@app.route('/treatment/<string:id>', methods=['DELETE'])
def delete_treatment(id):
    try:
        # Intentar obtener el registro antes de eliminarlo para verificar si existe
        consult_ref = firebase_db.get('/treatment', id)
        if consult_ref is None:
            return jsonify({'message': 'No patient found with the provided ID'}), 404

        # Eliminar el registro
        firebase_db.delete('/treatment', id)
        return jsonify({'message': f'treatment record with ID {id} deleted successfully'})

    except Exception as e:
        app.logger.error(f"Error: {str(e)}")  # Registrar el error en los logs del servidor
        return jsonify({'error': str(e)}), 500