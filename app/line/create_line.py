from datetime import datetime
from app.utils.firebase import firebase_db

from flask import request, jsonify
from app import app


@app.route('/line', methods=['POST'])
def create_line():
    try:
        data = request.get_json()

        result = firebase_db.post('/line', data)
        return jsonify({'message': ' line create success successfully', 'id': result['name']})
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@app.route('/line/<string:record_id>', methods=['PUT'])
def edit_line(record_id):
    try:
        data = request.get_json()
        data['last_modified_date'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        # Lista de campos a validar
        fields_to_validate = ["line", "line-2", "line-3", "line-4", "line-5", "line-6","line-7"]

        # Validación: Comprueba que cada campo esté en el rango de 1 a 3
        validation_failed = False

        for field in fields_to_validate:
            if field in data and not (1 <= data[field] <= 3):
                validation_failed = True
                break

        if validation_failed:
            return jsonify({'error': 'solo valores del 1 al 3'}), 400

        # Realiza una solicitud PUT para actualizar el registro en Firebase
        firebase_db.put('/line', record_id, data)

        return jsonify({'message': f'se actualizo correctamente'})
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@app.route('/line/<string:line_id>', methods=['GET'])
def get_line(line_id):
    try:
        line = firebase_db.get('/line', line_id)
        if line:
            return jsonify(line)
        else:
            return jsonify({'error': 'Linea no encontrada'}), 404
    except Exception as e:
        return jsonify({'error': str(e)}), 500
