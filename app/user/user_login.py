from flask import jsonify, request
from app import app
from app.utils.firebase import firebase_db
import bcrypt


@app.route('/user/login', methods=['POST'])
def login_user():
    try:
        data = request.get_json()

        users = firebase_db.get('/user', None)
        if users:
            for user_id, user_data in users.items():
                if user_data.get('email') == data.get('email'):
                    # Extraer la contraseña encriptada almacenada en la base de datos
                    stored_password = user_data.get('password')

                    # Verificar si la contraseña ingresada por el usuario coincide con la contraseña encriptada
                    if bcrypt.checkpw(data.get('password').encode('utf-8'), stored_password.encode('utf-8')):
                        # Las contraseñas coinciden, devuelve los campos de usuario deseados
                        selected_fields = {
                            'user_id': user_data.get('user_id'),
                            'last_name': user_data.get('last_name'),
                            'name': user_data.get('name'),
                            'phone': user_data.get('phone'),
                            'email': user_data.get('email'),
                            'creation_date': user_data.get('creation_date')
                        }
                        return jsonify({'message': 'User logged in successfully', 'data': selected_fields}), 200

        # Si no se encuentra un usuario coincidente o las contraseñas no coinciden, devuelve un mensaje de error.
        return jsonify({'message': 'Invalid credentials'}, 401)

    except Exception as e:
        return jsonify({'error': str(e)}), 500
