from datetime import datetime
from flask import jsonify, request
from app import app
from app.utils.firebase import firebase_db
import secrets
import bcrypt


@app.route('/user/register', methods=['POST'])
def register_user():
    try:
        data = request.get_json()
        data['creation_date'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        # Check if the user already exists in Firebase
        existing_user = None
        users = firebase_db.get('/user', None)
        if users:
            for user_id, user_data in users.items():
                if user_data.get('email') == data.get('email'):
                    existing_user = user_id
                    break

        if existing_user:
            return jsonify({'message': 'User already exists', 'id': existing_user}), 400

        # Generate a random token for the new user
        token = secrets.token_hex(16)  # Generates a 32-character (16 bytes) hexadecimal token
        data['user_id'] = token  # Add the token to the data dictionary

        # Encripta la contraseña antes de guardarla en la base de datos
        password = data.get('password').encode('utf-8')  # Convierte la contraseña en bytes
        hashed_password = bcrypt.hashpw(password, bcrypt.gensalt())
        data['password'] = hashed_password.decode('utf-8')  # Guarda la contraseña en su forma encriptada

        result = firebase_db.post('/user', data)
        return jsonify({'message': 'User record created successfully', 'id': result['name']}), 200

    except Exception as e:
        return jsonify({'error': str(e)}), 500
