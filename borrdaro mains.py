from flask import Flask


from flask import request, jsonify
from firebase import firebase
from app.utils.ConfigFirebase import firebaseConfig


firebase_db = firebase.FirebaseApplication(firebaseConfig['databaseURL'], None)

app = Flask(__name__)



@app.route('/crear_paciente', methods=['POST'])
def crear_paciente():
    try:
        data = request.get_json()
        result = firebase_db.post('/pacientes', data)
        return jsonify({'mensaje': 'Registro de paciente creado correctamente', 'id': result['name']})
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@app.route('/obtener_pacientes', methods=['GET'])
def obtener_pacientes():
    try:
        # Consulta todos los pacientes en la base de datos de Firebase
        result = firebase_db.get('/pacientes', None)

        if result:
            # Convierte los datos de Firebase a una lista de pacientes
            pacientes = list(result.values())
            return jsonify(pacientes)
        else:
            return jsonify([])  # Devuelve una lista vacía si no hay pacientes

    except Exception as e:
        return jsonify({'error': str(e)}), 500

@app.route('/actualizar_paciente/<string:paciente_id>', methods=['PUT', 'POST'])
def actualizar_paciente(paciente_id):
    try:
        # Obtén los datos JSON enviados en la solicitud
        data = request.get_json()

        # Actualiza el paciente en la base de datos de Firebase
        result = firebase_db.put('/pacientes', paciente_id, data)

        return jsonify({'mensaje': 'Registro de paciente actualizado correctamente'})
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@app.route('/borrar_paciente/<string:paciente_id>', methods=['DELETE'])
def borrar_paciente(paciente_id):
    try:
        # Realiza una solicitud de eliminación para borrar el paciente en la base de datos de Firebase
        result = firebase_db.delete('/pacientes', paciente_id)

        if result:
            return jsonify({'mensaje': 'Registro de paciente borrado correctamente'})
        else:
            return jsonify({'mensaje': 'No se encontró un paciente con el ID proporcionado'})

    except Exception as e:
        return jsonify({'error': str(e)}), 500



@app.route('/buscar_pacientes_por_nombre', methods=['GET'])
def buscar_pacientes_por_nombre():
    try:
        # Obtén el nombre desde la URL
        nombre = request.args.get('nombre')

        if not nombre:
            return jsonify({'error': 'Se requiere el parámetro "nombre" en la URL'})

        pacientes = firebase_db.get('/pacientes', None)
        pacientes_coincidentes = []

        if pacientes:
            for key, paciente in pacientes.items():
                if 'nombre' in paciente:
                    if nombre.lower() in paciente['nombre'].lower():
                        pacientes_coincidentes.append(paciente)

        if pacientes_coincidentes:
            return jsonify(pacientes_coincidentes)
        else:
            return jsonify({'mensaje': 'No se encontraron pacientes con el nombre especificado'})

    except Exception as e:
        return jsonify({'error': str(e)}), 500




if __name__ == '__main__':
    app.run(debug=True)
